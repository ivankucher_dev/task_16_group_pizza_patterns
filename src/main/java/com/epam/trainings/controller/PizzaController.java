package com.epam.trainings.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pizza")
public class PizzaController {

    @GetMapping("/getPizza")
    public String getAllPizza(){
        return "allPizza";
    }
}
