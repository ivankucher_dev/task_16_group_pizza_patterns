package com.epam.trainings.controller.impl;

import com.epam.trainings.controller.ChelentanoController;
import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.orderform.CustomPizzaOrderForm;
import com.epam.trainings.model.orderform.PizzaOrderForm;
import com.epam.trainings.model.pizza.Pizza;
import com.epam.trainings.pizzafactory.CityPizzaFactory;
import com.epam.trainings.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/pizza")
public class PizzaControllerImpl implements ChelentanoController {
  @Autowired private CityPizzaFactory factory;
  @Autowired private DataService dataService;

  @GetMapping
  public String getPizzaForm(Model model) {
    model.addAttribute("ingridients", dataService.getAllIngridients());
    model.addAttribute("cities", dataService.getAllCities());
    model.addAttribute("pizzas", dataService.getAllPizzasType());
    return "index";
  }

  @GetMapping("/orders")
  public String getOrdersList(Model model) {
    model.addAttribute("orders", dataService.getAllOrders());
    return "orders";
  }

  @PostMapping(value = "/create-pizza")
  public String createPizza(PizzaOrderForm form) {
    List<Ingridient> ingridients = dataService.getIngridients(form.getPizzaType(), form.getCity());
    Pizza pizza = factory.preparePizzaOrder(form.getPizzaType(),ingridients,Integer.parseInt(form.getSize()));
    factory.boxAndSendPizza(pizza,form.getCustomer());
    return "redirect:/pizza/orders";
  }

  @PostMapping(value = "/create-custom-pizza")
  public String createCustomPizza(CustomPizzaOrderForm form) {
    List<Ingridient> ingridients =
        dataService.getIngridientsByCity(form.getIngridients(), form.getCity());
    Pizza pizza = factory.preparePizzaOrder(ingridients,form.getSize());
    factory.boxAndSendPizza(pizza,form.getCustomer());
    return "redirect:/pizza/orders";
  }
}
