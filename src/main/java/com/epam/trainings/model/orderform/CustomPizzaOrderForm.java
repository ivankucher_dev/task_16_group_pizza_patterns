package com.epam.trainings.model.orderform;

import com.epam.trainings.model.ingridient.Ingridient;
import java.util.List;

public class CustomPizzaOrderForm {
  private String city;
  private int size;
  private List<String> ingridients;
  private String customer;

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public int getSize() {
    return size;
  }

  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public List<String> getIngridients() {
    return ingridients;
  }

  public void setIngridients(List<String> ingridients) {
    this.ingridients = ingridients;
  }
}
