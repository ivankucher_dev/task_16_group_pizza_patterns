package com.epam.trainings.model.orderform;

public class PizzaOrderForm {
  private String city;
  private String pizzaType;
  private String size;
  private String customer;

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPizzaType() {
    return pizzaType;
  }

  public void setPizzaType(String pizzaType) {
    this.pizzaType = pizzaType;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

  @Override
  public String toString() {
    return "PizzaOrderForm{" +
        "city='" + city + '\'' +
        ", pizzaType='" + pizzaType + '\'' +
        ", size='" + size + '\'' +
        '}';
  }
}
