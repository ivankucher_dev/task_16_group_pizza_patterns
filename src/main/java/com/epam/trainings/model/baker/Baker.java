package com.epam.trainings.model.baker;

import com.epam.trainings.model.pizza.Pizza;
import com.epam.trainings.model.pizzadb.PizzaStatus;
import com.epam.trainings.model.pizzaorder.PizzaOrder;
import com.epam.trainings.service.DataService;
import com.epam.trainings.utils.BeanUtil;

import static com.epam.trainings.utils.RandomSleep.getRandomNumberInRange;

public class Baker extends SimpleBaker {

  private DataService dataService;
  private static final int pizzasSlices = 8;
  private static final int minSleep = 10000;
  private static final int maxSleep = 20000;
  private int totalTime;

  public Baker(Pizza pizza, String customer) {
    this.pizza = pizza;
    this.pizzaOrder = new PizzaOrder(pizza.getPizzaType(), customer);
    dataService = BeanUtil.getBean(DataService.class);
    dataService.saveOrder(pizzaOrder);
  }

  @Override
  public void cut() {
    pizza.setNumOfSlices(pizzasSlices);
    Runnable myRunnable =
        () -> {
          try {
            totalTime = totalTime + getRandomNumberInRange(minSleep, maxSleep);
            Thread.sleep(totalTime);
            pizzaOrder.setStatus(PizzaStatus.Cutted);
            dataService.updateOrderStatus(pizzaOrder, pizzaOrder.getStatus().name());
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        };
    Thread cutter = new Thread(myRunnable);
    cutter.start();
  }

  @Override
  public void bake() {
    pizza.setSize(pizza.getSize() - 1);
    Runnable myRunnable =
        () -> {
          try {
            totalTime = totalTime + getRandomNumberInRange(minSleep, maxSleep);
            Thread.sleep(totalTime);
            pizzaOrder.setStatus(PizzaStatus.Baked);
            dataService.updateOrderStatus(pizzaOrder, pizzaOrder.getStatus().name());
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        };
    Thread cutter = new Thread(myRunnable);
    cutter.start();
  }

  @Override
  public void box() {
    Runnable myRunnable =
        () -> {
          try {
            totalTime = totalTime + getRandomNumberInRange(minSleep, maxSleep);
            Thread.sleep(totalTime);
            pizzaOrder.setStatus(PizzaStatus.Boxed);
            dataService.updateOrderStatus(pizzaOrder, pizzaOrder.getStatus().name());
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        };
    Thread cutter = new Thread(myRunnable);
    cutter.start();
  }
}
