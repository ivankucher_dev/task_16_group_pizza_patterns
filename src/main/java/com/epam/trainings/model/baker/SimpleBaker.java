package com.epam.trainings.model.baker;

import com.epam.trainings.model.pizza.Pizza;
import com.epam.trainings.model.pizzadb.PizzaMenu;
import com.epam.trainings.model.pizzaorder.PizzaOrder;

public abstract class SimpleBaker {
  protected Pizza pizza;
  protected PizzaOrder pizzaOrder;

  public abstract void cut();

  public abstract void bake();

  public abstract void box();
}
