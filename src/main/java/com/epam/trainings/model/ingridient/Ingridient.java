package com.epam.trainings.model.ingridient;


import com.epam.trainings.model.recipe.Recipe;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "ingridients")
public class Ingridient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int weight;
    @OneToMany(
            mappedBy = "ingridient",
            fetch = FetchType.LAZY
    )
    private List<Recipe> pizzaIngridients = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Recipe> getPizzaIngridients() {
        return pizzaIngridients;
    }

    public void setPizzaIngridients(List<Recipe> pizzaIngridients) {
        this.pizzaIngridients = pizzaIngridients;
    }

    public Ingridient(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public Ingridient(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Ingridient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight;
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Ingridient that = (Ingridient) object;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
