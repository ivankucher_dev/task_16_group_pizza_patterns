package com.epam.trainings.model.pizza;

import com.epam.trainings.model.ingridient.Ingridient;

import java.util.List;

public class Pizza {
  private String pizzaType;
  private int size;
  private int numOfSlices;
  private List<Ingridient> ingridients;

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getNumOfSlices() {
        return numOfSlices;
    }

    public void setNumOfSlices(int numOfSlices) {
        this.numOfSlices = numOfSlices;
    }

    public List<Ingridient> getIngridients() {
        return ingridients;
    }

    public void setIngridients(List<Ingridient> ingridients) {
        this.ingridients = ingridients;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        ingridients.forEach(ingridient -> sb.append(ingridient+"\n"));
        return "Pizza{" +
                "pizzaType='" + pizzaType + '\'' +
                ", size=" + size +
                ", numOfSlices=" + numOfSlices +
                ", ingridients={" +sb.toString() +
                '}';
    }
}
