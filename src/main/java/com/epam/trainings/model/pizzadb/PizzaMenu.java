package com.epam.trainings.model.pizzadb;

import com.epam.trainings.model.recipe.Recipe;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "pizza")
public class PizzaMenu {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String pizzaType;
  private int size;

  @OneToMany(mappedBy = "pizza", fetch = FetchType.LAZY)
  private List<Recipe> pizzaIngridients = new ArrayList<>();

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<Recipe> getPizzaIngridients() {
    return pizzaIngridients;
  }

  public void setPizzaIngridients(List<Recipe> pizzaIngridients) {
    this.pizzaIngridients = pizzaIngridients;
  }

  public String getPizzaType() {
    return pizzaType;
  }

  public void setPizzaType(String pizzaType) {
    this.pizzaType = pizzaType;
  }
}
