package com.epam.trainings.model.recipe;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class PizzaIngridient implements Serializable {
    @Column(name = "ingridient_id")
  private int ingridient_id;
    @Column(name = "pizza_id")
  private int pizza_id;

  public PizzaIngridient(int ingridient_id, int pizza_id) {
    this.ingridient_id = ingridient_id;
    this.pizza_id = pizza_id;
  }

  public PizzaIngridient(){}

  @ManyToOne(cascade = CascadeType.ALL)
  public int getIngridient() {
    return ingridient_id;
  }

  public void setIngridient(int ingridient) {
    this.ingridient_id = ingridient;
  }

  @ManyToOne(cascade = CascadeType.ALL)
  public int getPizza() {
    return pizza_id;
  }

  public void setPizza(int pizza) {
    this.pizza_id = pizza;
  }
}
