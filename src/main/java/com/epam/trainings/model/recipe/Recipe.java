package com.epam.trainings.model.recipe;

import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizzadb.PizzaMenu;

import javax.persistence.*;

@Entity(name = "Recipe")
@Table(name = "recipe")
public class Recipe {
    @EmbeddedId
    private PizzaIngridient primaryKey;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("ingridient")
    private Ingridient ingridient;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("pizza")
    private PizzaMenu pizza;
    @Column(name = "city_name")
    private String city;

    private Recipe() {}

    public Recipe(PizzaMenu pizzaMenu, Ingridient ingridient) {
       this.pizza = pizzaMenu;
       this.ingridient = ingridient;
        this.primaryKey = new PizzaIngridient(pizzaMenu.getId(), ingridient.getId());
    }
    public PizzaIngridient getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PizzaIngridient primaryKey) {
        this.primaryKey = primaryKey;
    }


    @Transient
    public int getPizza() {
        return getPrimaryKey().getPizza();
    }

    @Transient
    public int getIngridient() {
        return getPrimaryKey().getIngridient();
    }

    public void setPizza(int pizzaMenu){
        getPrimaryKey().setPizza(pizzaMenu);
    }

    public void setIngridient(int ingridient){
        getPrimaryKey().setIngridient(ingridient);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


}
