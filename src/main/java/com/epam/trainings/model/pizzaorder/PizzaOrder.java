package com.epam.trainings.model.pizzaorder;

import com.epam.trainings.model.pizza.Pizza;
import com.epam.trainings.model.pizzadb.PizzaStatus;

import javax.persistence.*;

@Entity
@Table(name = "pizza_order")
public class PizzaOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String pizzaType;
    private String customer;
    private String status;

    public PizzaOrder(String pizzaType,String customer) {
        this.pizzaType = pizzaType;
        this.status = PizzaStatus.Preparing.name();
        this.customer = customer;
    }


    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public PizzaOrder() {
        this.status = PizzaStatus.Preparing.name();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public PizzaStatus getStatus() {
        return PizzaStatus.valueOf(status);
    }

    public void setStatus(PizzaStatus status) {
        this.status = status.name();
    }
}
