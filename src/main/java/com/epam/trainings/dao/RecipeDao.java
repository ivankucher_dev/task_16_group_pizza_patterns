package com.epam.trainings.dao;

import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizzadb.PizzaMenu;
import com.epam.trainings.model.pizzaorder.PizzaOrder;

import java.util.List;

public interface RecipeDao {
    Integer getPizzaByType(String pizzaType);
    List<Ingridient> getIngridientsByCity(String pizzaType, String city);
    Ingridient getIngridientById(int id);
    List<Ingridient> getIngridientsByCity(List<String> ingridients, String city);
    List getAllIngridients();
    List<String> getAllCities();
    List<PizzaMenu> getAllPizzas();
    void updateOrderStatus(PizzaOrder order , String status);
    void saveOrder(PizzaOrder order);
    List<PizzaOrder> getAllOrders();
}
