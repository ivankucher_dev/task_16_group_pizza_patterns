package com.epam.trainings.dao;

import com.epam.trainings.model.city.City;
import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizzadb.PizzaMenu;
import com.epam.trainings.model.pizzaorder.PizzaOrder;
import com.epam.trainings.model.recipe.Recipe;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static com.epam.trainings.utils.HibernateSessionFactoryUtil.getSessionFactory;
import static com.epam.trainings.utils.PropertiesReader.getPropertiesFile;

@Repository
public class RecipeDaoImpl implements RecipeDao {
  private Properties queryProperties = getPropertiesFile("query.properties");


  @Override
  public Integer getPizzaByType(String pizzaType) {
    Session session = getSessionFactory().openSession();
    session.getTransaction().begin();
    Query query = session.createQuery(queryProperties.getProperty("get_pizza_by_type"));
    query.setParameter("pizzaType", pizzaType);
    if (query.list().size() == 0) {
      return 0;
    }
    PizzaMenu pizzaMenu = (PizzaMenu) query.list().get(0);
    session.getTransaction().commit();
    session.close();
    return pizzaMenu.getId();
  }

  @Override
  public List<Ingridient> getIngridientsByCity(String pizzaType, String city) {
    Session session = getSessionFactory().openSession();
    session.getTransaction().begin();
    int pizzaId = getPizzaByType(pizzaType);
    Query query =
        session.createQuery(queryProperties.getProperty("get_ingridients_by_pizzatype_and_city"));
    query.setParameter("pizzaid", pizzaId);
    query.setParameter("city", city);
    List<Ingridient> ingridients = new ArrayList<>();
    List<Recipe> recipes = (List<Recipe>) query.list();
    recipes.forEach(recipe -> ingridients.add(getIngridientById(recipe.getIngridient())));
    session.getTransaction().commit();
    session.close();
    return ingridients;
  }

  @Override
  public Ingridient getIngridientById(int id) {
    Session session = getSessionFactory().openSession();
    session.getTransaction().begin();
    Ingridient ingridient = session.get(Ingridient.class, id);
    session.getTransaction().commit();
    session.close();
    return ingridient;
  }

  @Override
  public List<Ingridient> getIngridientsByCity(List<String> ingridients, String city) {
    Session session = getSessionFactory().openSession();
    session.getTransaction().begin();
    List<Ingridient> ingridientObjects = new ArrayList<>();
    Query query =
        session.createQuery(queryProperties.getProperty("get_ingridient_id_by_name_and_city"));
    query.setParameter("city", city);
    query.setMaxResults(1);
    for (String ingridient : ingridients) {
      query.setParameter("name", ingridient);
      final List<Recipe> list = new LinkedList<>();
      for (final Object o : query.list()) {
        ingridientObjects.add(getIngridientById(((Recipe) o).getIngridient()));
      }
    }
    return ingridientObjects;
  }

  @Override
  public List<Ingridient> getAllIngridients() {
    return getSessionFactory()
        .openSession()
        .createQuery(queryProperties.getProperty("get_all_ingridients"))
        .list();
  }

  @Override
  public List<String> getAllCities() {
    Session session = getSessionFactory().openSession();
    session.getTransaction().begin();
    Query query =
            session.createQuery("from City");
    List<City> cities = query.list();
    return cities.stream().map(city -> city.getName()).collect(Collectors.toList());
  }

  @Override
  public List<PizzaMenu> getAllPizzas() {
    return getSessionFactory()
            .openSession()
            .createQuery("from PizzaMenu")
            .list();
  }

  @Override
  public void updateOrderStatus(PizzaOrder order,String status) {
    Session session = getSessionFactory().openSession();
    session.getTransaction().begin();
    Query query = session.createQuery(queryProperties.getProperty("update_pizza_order"));
    query.setParameter("status", status);
    query.setParameter("customer", order.getCustomer());
    query.setParameter("pizzaType", order.getPizzaType());

    query.executeUpdate();
    session.getTransaction().commit();
    session.close();
  }

  @Override
  public void saveOrder(PizzaOrder order) {
    Session session = getSessionFactory().openSession();
    session.getTransaction().begin();
    session.save(order);
    session.getTransaction().commit();
    session.close();
  }

  @Override
  public List<PizzaOrder> getAllOrders() {
    return getSessionFactory()
            .openSession()
            .createQuery("from PizzaOrder")
            .list();
  }
}
