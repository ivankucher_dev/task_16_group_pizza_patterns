package com.epam.trainings.service;

import com.epam.trainings.dao.RecipeDao;
import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizza.Pizza;
import com.epam.trainings.model.pizzadb.PizzaMenu;
import com.epam.trainings.model.pizzaorder.PizzaOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataService implements DBService {
  @Autowired private RecipeDao recipeDao;

  public void setRecipeDao(RecipeDao recipeDao) {
    this.recipeDao = recipeDao;
  }

  @Override
  public Integer getPizzaByType(String pizzaType) {
    return recipeDao.getPizzaByType(pizzaType);
  }

  @Override
  public List<Ingridient> getIngridients(String pizzaType, String city) {
    return recipeDao.getIngridientsByCity(pizzaType, city);
  }

  @Override
  public List<Ingridient> getIngridientsByCity(List<String> ingridients, String city) {
    return recipeDao.getIngridientsByCity(ingridients, city);
  }

  @Override
  public List<Ingridient> getAllIngridients() {
    return new ArrayList<>(new HashSet<Ingridient>(recipeDao.getAllIngridients()));
  }

  @Override
  public List<String> getAllCities() {
    return recipeDao.getAllCities();
  }

  @Override
  public List<PizzaMenu> getAllPizzas() {
    return recipeDao.getAllPizzas();
  }

  @Override
  public List<String> getAllPizzasType() {
    return recipeDao.getAllPizzas().stream()
        .map(pizza -> pizza.getPizzaType())
        .collect(Collectors.toList());
  }

  @Override
  public void updateOrderStatus(PizzaOrder order, String status) {
    recipeDao.updateOrderStatus(order,status);
  }

  @Override
  public void saveOrder(PizzaOrder order) {
    recipeDao.saveOrder(order);
  }

  @Override
  public List<PizzaOrder> getAllOrders() {
    return recipeDao.getAllOrders();
  }
}
