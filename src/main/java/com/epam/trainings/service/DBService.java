package com.epam.trainings.service;

import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizzadb.PizzaMenu;
import com.epam.trainings.model.pizzaorder.PizzaOrder;

import java.util.List;

public interface DBService {
    Integer getPizzaByType(String pizzaType);
    List<Ingridient> getIngridients(String pizzaType, String city);
    List<Ingridient> getIngridientsByCity(List<String> ingridients,String city);
    List<Ingridient> getAllIngridients();
    List<String> getAllCities();
    List<PizzaMenu> getAllPizzas();
    List<String> getAllPizzasType();
    void updateOrderStatus(PizzaOrder order , String status);
    void saveOrder(PizzaOrder order);
    List<PizzaOrder> getAllOrders();
}
