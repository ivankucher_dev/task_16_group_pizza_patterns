package com.epam.trainings.utils;

import com.epam.trainings.model.city.City;
import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizzadb.PizzaMenu;
import com.epam.trainings.model.pizzaorder.PizzaOrder;
import com.epam.trainings.model.recipe.PizzaIngridient;
import com.epam.trainings.model.recipe.Recipe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {

  public static SessionFactory sessionFactory;
  private static Logger log = LogManager.getLogger(HibernateSessionFactoryUtil.class.getName());

  public static SessionFactory getSessionFactory() {
    if (sessionFactory == null) {
      try {
        Configuration configuration = new Configuration().configure();
        configuration.addAnnotatedClass(PizzaMenu.class);
        configuration.addAnnotatedClass(Ingridient.class);
        configuration.addAnnotatedClass(Recipe.class);
        configuration.addAnnotatedClass(PizzaIngridient.class);
        configuration.addAnnotatedClass(City.class);
        configuration.addAnnotatedClass(PizzaOrder.class);
        StandardServiceRegistryBuilder bulilder =
            new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(bulilder.build());
      } catch (Exception e) {
        log.error(e);
      }
    }
    return sessionFactory;
  }
}
