package com.epam.trainings.pizzafactory;

import com.epam.trainings.model.baker.Baker;
import com.epam.trainings.model.baker.SimpleBaker;
import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizza.Pizza;

import java.util.List;

public abstract class CityPizzaFactory {
    public abstract Pizza preparePizzaOrder(String pizzaType, List<Ingridient> ingridients,int size);

    public abstract Pizza preparePizzaOrder(List<Ingridient> ingridient,int size);

    public Pizza boxAndSendPizza(Pizza pizza,String customer) {
        SimpleBaker baker = new Baker(pizza,customer);
        baker.bake();
        baker.cut();
        baker.box();
        return pizza;
    }


}
