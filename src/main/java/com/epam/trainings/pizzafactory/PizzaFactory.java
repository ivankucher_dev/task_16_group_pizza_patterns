package com.epam.trainings.pizzafactory;

import com.epam.trainings.model.ingridient.Ingridient;
import com.epam.trainings.model.pizza.Pizza;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PizzaFactory extends CityPizzaFactory {
  @Override
  public Pizza preparePizzaOrder(String pizzaType, List<Ingridient> ingridients,int size) {
    Pizza pizza = new Pizza();
    pizza.setPizzaType(pizzaType);
    pizza.setIngridients(ingridients);
    pizza.setSize(size);
    return pizza;
  }

  @Override
  public Pizza preparePizzaOrder(List<Ingridient> ingridient,int size) {
    Pizza pizza = new Pizza();
    pizza.setPizzaType("custom pizza");
    pizza.setIngridients(ingridient);
    pizza.setSize(size);
    return pizza;
  }
}
