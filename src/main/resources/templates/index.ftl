<!DOCTYPE html>
<#import "/spring.ftl" as spring/>
<html>

<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <title>Page Title</title>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel="stylesheet" type="text/css"
        href="css/styles.css"/>
  <link rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
</head>

<body>
<div class="order-container">
  <div class="order">
    <form action="/pizza/create-pizza" method="post" id="pizza-form">
      <div class="form-group">
        <label for="city">Choose your city</label>
        <select class="form-control" id="city" name="city">
            <#list cities as city>
              <option value="${city}">${city}</option>
            </#list>
        </select>
      </div>
      <div class="form-group" id="menu-form">
        <label for="pizza-type">Choose your pizza</label>
        <select class="form-control" id="pizza-type" name="pizzaType">
            <#list pizzas as type>
              <option value="${type}">${type}</option>
            </#list>
        </select>
      </div>
      <div class="form-group" id="custom-form">
        <label for="">Add ingredients</label>
        <div class="form-check">
          <fieldset>
              <#list ingridients as ingredient>
                <input type="checkbox" name="ingridients"
                       value="${ingredient.name}">${ingredient.name}<br>
              </#list>
          </fieldset>
        </div>
      </div>
      <div class="form-group">
        <label for="pizza-size">Choose your pizza's size</label>
        <select class="form-control" id="pizza-size" name="size">
          <option name="size" value="30">Small (30cm)</option>
          <option name="size" value="35">Medium (35cm)</option>
          <option name="size" value="40">Large (40cm)</option>
        </select>
        <p>Enter your phone number : </p>
        <input type="text" name="customer">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  <div class="choice">
    <button type="button" onclick="menu()" class="choice-btn" id='menu'>Pizza</button>
    <button type="button" onclick="custom()" class="choice-btn" id="custom">Custom pizza</button>
  </div>
  <div class="order-list">
    <a href="/pizza/orders">Order list</a>
  </div>
</div>
</body>
<script>
  function custom() {
    document.getElementById('menu-form').style['display'] = 'none';
    document.getElementById('custom-form').style['display'] = 'block';
    document.getElementById('custom').style['backgroundColor'] = '#c0bdbd';
    document.getElementById('menu').style['backgroundColor'] = '#f2f2f2';
    document.getElementById('pizza-form').action = '/pizza/create-custom-pizza';
  }

  function menu() {
    document.getElementById('menu-form').style['display'] = 'block';
    document.getElementById('custom-form').style['display'] = 'none';
    document.getElementById('menu').style['backgroundColor'] = '#c0bdbd';
    document.getElementById('custom').style['backgroundColor'] = '#f2f2f2';
  }
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous">
</script>

</html>